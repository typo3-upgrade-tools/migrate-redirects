<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'MigrateRedirects',
	'description' => 'Upgrade wizard to migrate redirects from RealUrl and MyRedirects to core redirects',
	'category' => 'module',
	'author' => 'Jigal van Hemert',
	'author_email' => 'jigal.van.hemert@typo3.org',
	'author_company' => '',
	'state' => 'beta',
	'uploadfolder' => 0,
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '0.1.1',
	'constraints' => [
		'depends' => [
			'typo3' => '9.5.0-9.5.999',
            'php' => '7.2.0-7.3.999',
        ],
		'conflicts' => [],
		'suggests' => [],
    ],
];